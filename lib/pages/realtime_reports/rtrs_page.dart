import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/services/odoo_api.dart';
import 'package:vorazrtr/data/blocs/rtr_bloc.dart';
import 'package:vorazrtr/pages/realtime_reports/rtrs_listview.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/event_bus.dart';

class RtrsPage extends StatefulWidget {
  @override
  _RtrsPageState createState() => _RtrsPageState();
}

class _RtrsPageState extends Base<RtrsPage>
    with AutomaticKeepAliveClientMixin<RtrsPage> {
  StreamSubscription<Event> subscription;

  final _bloc = RtrBloc();

  @override
  bool get wantKeepAlive => true;

  Future<Odoo> odooInstance() async {
    Odoo odoo = await getOdooInstance();
    return odoo;
  }

  @override
  void initState() {
    super.initState();

    Base b = this;

    //pode ser usado para trazer prospect/cliente no futuro, so passa o tipo
    _bloc.fetch(b);

    final bus = EventBus.get(context);
    subscription = bus.stream.listen((Event e) {
      _bloc.fetch(b);
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return alert(context, "Não foi possível buscar os RTRs!");
        }

        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        List<Rtr> rtrs = snapshot.data;

        return RefreshIndicator(
          onRefresh: _onRefresh,
          child: RtrsListview(rtrs),
        );
      },
    );
  }

  Future<void> _onRefresh() {
    return _bloc.fetch(this);
  }

  @override
  void dispose() {
    super.dispose();

    _bloc.dispose();
    subscription.cancel();
  }
}
