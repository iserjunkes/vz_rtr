import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/my_text.dart';

class RtrPage extends StatelessWidget {
  Rtr rtr;

  RtrPage(this.rtr);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(rtr.name),
      ),
      body: _body(),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () => alert(context, "Clicou"),
      ),
    );
  }

  _body() {
    return Container(
      padding: EdgeInsets.all(16),
      child: ListView(
        children: <Widget>[
          _rtrDetails(),
          Divider(),
          _visits(),
        ],
      ),
    );
  }

  _rtrDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Column(
            children: <Widget>[
              CachedNetworkImage(
                  imageUrl: rtr.farm.imageUrl ??
                      "https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-512.png"),
              _textViews(),
            ],
          ),
        )
      ],
    );
  }

  _visits() {
    return Column();
  }

  _textViews() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Flexible(
          child: Column(
            children: <Widget>[
              text(rtr.name, fontSize: 20, bold: true),
              text("Fazenda: ${rtr.farm.name}"),
              text("Campanha: ${rtr.campaign.name}", fontSize: 16),
              text("Estado: ${rtr.state.name}", fontSize: 16),
              text("Cidade: ${rtr.city.name}", fontSize: 16),
            ],
          ),
        )
      ],
    );
  }
}
