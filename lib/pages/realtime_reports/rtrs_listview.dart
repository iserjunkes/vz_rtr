import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_form_page.dart';
import 'package:vorazrtr/pages/realtime_reports/rtr_page.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/nav.dart';

class RtrsListview extends StatelessWidget {
  final List<Rtr> _rtrs;

  RtrsListview(this._rtrs);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Relatórios em tempo-real'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _rtrs.length,
          itemBuilder: (context, index) {
            return _rtrCard(context, index);
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () => push(context, RtrFormPage()),
      ),
    );
  }

  _rtrCard(BuildContext context, int i) {
    return GestureDetector(
      onTap: () => _onClickOpenRtr(context, i),
      child: Card(
        margin: EdgeInsets.only(left: 5, top: 5, right: 8, bottom: 5),
        child: Column(
          children: <Widget>[
            ListTile(
              leading: CircleAvatar(
                foregroundColor: Theme.of(context).primaryColor,
                backgroundColor: Colors.grey,
                backgroundImage: NetworkImage((_rtrs[i].farm != null &&
                        _rtrs[i].farm.imageUrl != null)
                    ? _rtrs[i].farm.imageUrl
                    : "https://cdn.iconscout.com/icon/free/png-512/avatar-380-456332.png"),
              ),
              title: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      _rtrs[i].name,
                      style: TextStyle(fontWeight: FontWeight.bold),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              subtitle: Container(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  _rtrs[i].farm != null ? _rtrs[i].farm.name : "N/A",
                  style: TextStyle(color: Colors.grey, fontSize: 15.0),
                ),
              ),
            ),
            ButtonBarTheme(
              data: ButtonBarThemeData(buttonTextTheme: ButtonTextTheme.accent),
              child: ButtonBar(
                children: <Widget>[
                  FlatButton(
                      onPressed: () {
                        _onClickOpenRtr(context, i);
                      },
                      child: Text("Detalhes")),
                  FlatButton(
                      onPressed: () {
                        alert(context, "Em breve...", hideCancelBtn: true);
                      },
                      child: Text("Nova visita"))
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void _onClickOpenRtr(context, int i) {
    push(context, RtrPage(_rtrs[i]));
  }

}
