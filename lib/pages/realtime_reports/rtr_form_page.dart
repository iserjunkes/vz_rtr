import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/rtr_api.dart';
import 'package:vorazrtr/data/daos/campaign_dao.dart';
import 'package:vorazrtr/data/daos/city_dao.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/data/daos/state_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/rtr.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/data/services/odoo_response.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/nav.dart';
import 'package:vorazrtr/widgets/app_textfield.dart';

class RtrFormPage extends StatefulWidget {
  @override
  _RtrFormPageState createState() => _RtrFormPageState();
}

class _RtrFormPageState extends Base<RtrFormPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final tName = TextEditingController();
  final tFarm = TextEditingController();
  final tCampaign = TextEditingController();
  final List<DropdownMenuItem> partnerItemsDropdown = [];
  final List<DropdownMenuItem> campaignItemsDropdown = [];
  final List<DropdownMenuItem> stateItemsDropdown = [];
  final List<DropdownMenuItem> cityItemsDropdown = [];
  final PartnerDao partnerDao = PartnerDao();
  final CampaignDao campaignDao = CampaignDao();
  final CityDao cityDao = CityDao();
  final StateDao stateDao = StateDao();
  String _partnerName, _campaignName, _stateName, _cityName;
  Partner partnerSelected = Partner();
  Campaign campaignSelected = Campaign();
  CountryState stateSelected = CountryState();
  City citySelected = City();

  @override
  void initState() {
    super.initState();

    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Novo RTR"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            tooltip: "Salvar",
            onPressed: _onClickSave,
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.all(16),
        child: _form(),
      ),
    );
  }

  _form() {
    return Form(
      key: this._formKey,
      child: ListView(
        children: <Widget>[
          Container(
            child: partnerSelected.imageUrl != null
                ? CachedNetworkImage(
                    width: 150, height: 150, imageUrl: partnerSelected.imageUrl)
                : _img(),
          ),
          Divider(),
          AppTextfield(
            "Nome para o relatório",
            invisible: false,
            controller: tName,
            validator: _validRequired,
            textCapitalization: TextCapitalization.sentences,
          ),
//          AppText(
//            "Nome para o relatorio",
//            controller: tName,
//            validator: _validRequired,
//          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Fazenda:"),
            items: partnerItemsDropdown,
            value: _partnerName,
            hint: "Selecione a fazenda",
            searchHint: "Fazenda: ",
            onChanged: (value) {
              setState(() {
                _partnerName = value.name;
                partnerSelected = value;
              });
            },
            isExpanded: true,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Campanha:"),
            items: campaignItemsDropdown,
            value: _campaignName,
            hint: "Selecione a campanha",
            searchHint: "Campanha: ",
            onChanged: (value) {
              setState(() {
                _campaignName = value.name;
                campaignSelected = value;
              });
            },
            isExpanded: true,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Estado:"),
            items: stateItemsDropdown,
            value: _stateName,
            hint: "Selecione o estado",
            searchHint: "Estado: ",
            onChanged: (value) {
              setState(() {
                _stateName = value.name;
                stateSelected = value;
                _getData();
              });
            },
            isExpanded: true,
          ),
          Divider(),
          SearchableDropdown.single(
            label: Text("Cidade:"),
            items: cityItemsDropdown,
            value: _cityName,
            hint: "Selecione a cidade",
            searchHint: "Cidade: ",
            onChanged: (value) {
              setState(() {
                _cityName = value.name;
                citySelected = value;
              });
            },
            isExpanded: true,
          )
        ],
      ),
    );
  }

  Image _img() {
    return Image.asset(
      "assets/images/farmer.png",
      fit: BoxFit.contain,
      width: 150,
      height: 150,
    );
  }

  _getData() async {
    List<Partner> partnerDaoList = await partnerDao.findAll();
    List<Campaign> campaignDaoList = await campaignDao.findCampaigns();
    List<CountryState> stateDaoList = await stateDao.findAll();
    List<City> cityDaoList = await cityDao.findCityByState(stateSelected.id);

    setState(() {
      //alimenta lista de clientes
      for (Partner p in partnerDaoList) {
        partnerItemsDropdown.add(DropdownMenuItem(
          child: Text(p.name),
          value: p,
        ));
      }
      //alimenta lista de campanhas
      for (Campaign c in campaignDaoList) {
        campaignItemsDropdown.add(DropdownMenuItem(
          child: Text(c.name),
          value: c,
        ));
      }
      //alimenta lista de estados
      for (CountryState state in stateDaoList) {
        stateItemsDropdown.add(DropdownMenuItem(
          child: Text(state.name),
          value: state,
        ));
      }
      //alimenta lista de cidades
      for (City city in cityDaoList) {
        cityItemsDropdown.add(DropdownMenuItem(
          child: Text(city.name),
          value: city,
        ));
      }
    });
  }

  String _validRequired(String value) {
    if (value == null || value.isEmpty) {
      return 'Campo obrigatório';
    }

    return null;
  }

  _onClickSave() async {
    bool formOk = _formKey.currentState.validate();

    if (!formOk) {
      return;
    }

    var rtr = Rtr(
        name: tName.text,
        farm: partnerSelected,
        campaign: campaignSelected,
        state: stateSelected,
        city: citySelected);

    OdooResponse response = await RtrApi.saveRtr(this, rtr.toMap());

    if (response != null) {
      alert(context, response.getResult());
    } else {
      alert(context, "Salvo com sucesso!",
          hideCancelBtn: true, callback: () => pop(context));
    }
  }
}
