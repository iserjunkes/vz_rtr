import 'dart:async';

import 'package:flutter/material.dart';
import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/services/odoo_api.dart';
import 'package:vorazrtr/data/blocs/partner_bloc.dart';
import 'package:vorazrtr/pages/partners/partners_listview.dart';
import 'package:vorazrtr/utils/alert.dart';
import 'package:vorazrtr/utils/event_bus.dart';

class PartnersPage extends StatefulWidget {
  @override
  _PartnersPageState createState() => _PartnersPageState();
}

class _PartnersPageState extends Base<PartnersPage>
    with AutomaticKeepAliveClientMixin<PartnersPage> {
  StreamSubscription<Event> subscription;

  final _bloc = PartnerBloc();

  @override
  bool get wantKeepAlive => true;

  Future<Odoo> odooInstance() async {
    Odoo odoo = await getOdooInstance();
    return odoo;
  }

  @override
  void initState() {
    super.initState();

    Base b = this;

    //pode ser usado para trazer prospect/cliente no futuro, so passa o tipo
    _bloc.fetch(b);

    final bus = EventBus.get(context);
    subscription = bus.stream.listen((Event e) {
      _bloc.fetch(b);
    });
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _bloc.stream,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          return alert(context, "Não foi possível buscar os clientes");
        }

        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }

        List<Partner> partners = snapshot.data;

        return RefreshIndicator(
          onRefresh: _onRefresh,
          child: PartnersListview(partners),
        );
      },
    );
  }

  Future<void> _onRefresh() {
    return _bloc.fetch(this);
  }

  @override
  void dispose() {
    super.dispose();

    _bloc.dispose();
    subscription.cancel();
  }
}
