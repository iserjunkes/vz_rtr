import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/pages/partners/partner_page.dart';
import 'package:vorazrtr/utils/nav.dart';

class PartnersListview extends StatelessWidget {
  final List<Partner> _partners;

  PartnersListview(this._partners);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Clientes'),
      ),
      body: Container(
        child: ListView.builder(
          itemCount: _partners.length,
          itemBuilder: (context, index) {
//        Partner p = _partners[index];

            return _partnerCard(context, index);
          },
        ),
      ),
    );
  }

  Card _partnerCard(BuildContext context, int i) {
    return Card(
      margin: EdgeInsets.only(left: 5, top: 5, right: 8, bottom: 5),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: CircleAvatar(
              foregroundColor: Theme.of(context).primaryColor,
              backgroundColor: Colors.grey,
              backgroundImage: NetworkImage(_partners[i].imageUrl),
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  child: Text(
                    _partners[i].name,
                    style: TextStyle(fontWeight: FontWeight.bold),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
            subtitle: Container(
              padding: const EdgeInsets.only(top: 5.0),
              child: Text(
                _partners[i].email,
                style: TextStyle(color: Colors.grey, fontSize: 15.0),
              ),
            ),
          ),
          ButtonBarTheme(
            data: ButtonBarThemeData(buttonTextTheme: ButtonTextTheme.accent),
            child: ButtonBar(
              children: <Widget>[
                FlatButton(
                    onPressed: () {
                      _onClickOpenPartner(context, i);
                    },
                    child: Text("Detalhes")),
                FlatButton(
                    onPressed: () {
                      print("Novo RTR");
                    },
                    child: Text("Novo RTR"))
              ],
            ),
          )
        ],
      ),
    );
  }

  void _onClickOpenPartner(context, int i) {
    push(context, PartnerPage(_partners[i]));
  }
}
