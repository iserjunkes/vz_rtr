import 'package:flutter/material.dart';

class AppTextfield extends StatelessWidget {
  String text;
  String errorMessage;
  bool invisible;
  TextEditingController controller;
  TextInputAction textInputAction;
  FocusNode focusNode;
  FocusNode nextFocus;
  TextCapitalization textCapitalization;
  final FormFieldValidator<String> validator;

  AppTextfield(this.text,
      {@required this.invisible,
      this.controller,
      this.textInputAction,
      this.focusNode,
      this.nextFocus,
      this.validator,
      this.textCapitalization = TextCapitalization.none,
      this.errorMessage});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      textCapitalization: textCapitalization,
      validator: validator,
      textInputAction: textInputAction,
      focusNode: focusNode,
      onFieldSubmitted: (String text) {
        if (nextFocus != null) {
          FocusScope.of(context).requestFocus(nextFocus);
        }
      },
      decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 0.0)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
        labelText: text,
        labelStyle: TextStyle(
          fontSize: 20,
          color: Colors.green,
        ),
      ),
      obscureText: invisible,
      keyboardType: _inputType(invisible),
    );
  }

  _inputType(bool invisible) {
    if (invisible) {
      return TextInputType.text;
    } else {
      return TextInputType.emailAddress;
    }
  }
}
