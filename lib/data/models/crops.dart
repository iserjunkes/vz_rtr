import 'package:vorazrtr/utils/sql/entity.dart';

class Crops extends Entity {
  int id;
  String name;
  bool active;

  Crops({this.id, this.name, this.active});

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['active'] = this.active;
    return data;
  }
}
