import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/data/models/crops.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/models/stage.dart';
import 'package:vorazrtr/data/models/state.dart';
import 'package:vorazrtr/utils/sql/entity.dart';

class Rtr extends Entity {
  int id;
  String name;
  String reportName;
  Partner farm;
  Campaign campaign;
  CountryState state;
  City city;
  List<Rtr> visits;
  String type;
  bool isRtr;

  //visits attributes
  DateTime visitDate;
  Crops crops;
  Stage stage;
  String description;
  String finalConsiderations;


  Rtr({this.id, this.name, this.reportName, this.farm, this.campaign, this.state, this.city,
      this.visits, this.type, this.isRtr, this.visitDate, this.crops,
      this.stage, this.description, this.finalConsiderations});

  Rtr.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    name = map['name'];
    reportName = map['report_name'];
    farm = map['partner_id'];
    campaign = map['campaign_id'];
    state = map['state_id'];
    city = map['city_id'];
    type = map['type'];
    isRtr = map['is_rtr'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['report_name'] = this.name;
    data['partner_id'] = this.farm.id;
    data['campaign_id'] = this.campaign != null ? this.campaign.id : null;
    data['state_id'] = this.state != null ? this.state.id : null;
    data['city_id'] = this.city.id != null ? this.city.id : null;
    data['type'] = "rtr";
    data['is_rtr'] = true;
    return data;
  }
}
