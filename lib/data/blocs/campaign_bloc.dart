import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/campaign_api.dart';
import 'package:vorazrtr/data/daos/campaign_dao.dart';
import 'package:vorazrtr/data/models/campaign.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class CampaignBloc extends SimpleBloc<List<Campaign>> {
  Base base;

  Future<List<Campaign>> fetch(Base b) async {
    try {
      base = b;

      List<Campaign> campaigns = await CampaignApi.getCampaigns(base);

      final dao = CampaignDao();

      campaigns.forEach((p) => dao.save(p));

      add(campaigns);

      return campaigns;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}
