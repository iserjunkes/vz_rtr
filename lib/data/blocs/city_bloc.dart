import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/apis/city_api.dart';
import 'package:vorazrtr/data/daos/city_dao.dart';
import 'package:vorazrtr/data/models/city.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class CityBloc extends SimpleBloc<List<City>> {
  Base base;

  Future<List<City>> fetch(Base b) async {
    try {
      base = b;

      List<City> cities = await CityApi.getCities(base);

      final dao = CityDao();

      cities.forEach((p) => dao.save(p));

      add(cities);

      return cities;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}