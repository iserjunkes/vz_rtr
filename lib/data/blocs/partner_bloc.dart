import 'package:vorazrtr/base.dart';
import 'package:vorazrtr/data/models/partner.dart';
import 'package:vorazrtr/data/apis/partner_api.dart';
import 'package:vorazrtr/data/daos/partner_dao.dart';
import 'package:vorazrtr/utils/simple_bloc.dart';

class PartnerBloc extends SimpleBloc<List<Partner>> {
  Base base;

  Future<List<Partner>> fetch(Base b) async {
    try {
      base = b;

      List<Partner> partners = await PartnerApi.getPartners(base);

      final dao = PartnerDao();

      partners.forEach((p) => dao.save(p));

      add(partners);

      return partners;
    } catch (e) {
      addError(e);
      return [];
    }
  }
}
