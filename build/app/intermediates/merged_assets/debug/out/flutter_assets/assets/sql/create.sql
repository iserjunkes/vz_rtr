CREATE TABLE state(id INTEGER PRIMARY KEY, name TEXT, code TEXT, country_id INTEGER);
CREATE TABLE city(id INTEGER PRIMARY KEY, name TEXT, state_id INTEGER);
CREATE TABLE campaign(id INTEGER PRIMARY KEY, name TEXT);
CREATE TABLE rtr(id INTEGER PRIMARY KEY, report_name TEXT, name TEXT, campaign_id INTEGER, partner_id INTEGER,
state_id INTEGER, city_id INTEGER, type TEXT, is_rtr Text, visit_date DATE, crops_id INTEGER, stage_id INTEGER,
description TEXT, final_considerations TEXT);
CREATE TABLE partner(id INTEGER PRIMARY KEY, name TEXT, email TEXT, phone TEXT, address TEXT, imageUrl TEXT);